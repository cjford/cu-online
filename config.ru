require 'bundler'
Bundler.require

['lib', 'controllers'].each do |dir|
  Dir[File.expand_path("../#{dir}/*.rb", __FILE__)].sort.each do |file|
    require file
  end
end

map('/') {run Application}
map('/metro-enrollments') {run MetroEnrollments}
map('/auth') {run Auth}
