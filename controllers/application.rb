class Application < Sinatra::Base
  register Sinatra::ConfigFile
  register Sinatra::Flash
  register Sinatra::ApplicationHelper
  helpers  Sinatra::ApplicationHelper

  # Overrides default environments - uses staging instead of test
  set :environments, %w{development staging production}

  def self.config_path
    File.join(root, '..', 'config', "#{self.to_s.downcase}.yml")
  end

  # Loads the default config, which can be overridden by each controller
  # by including this line at the top of the class along with the matching
  # config/controllername.yml file
  config_file self.config_path

  configure :production, :staging do
    use Rack::SslEnforcer
    use Rack::Session::Cookie,
      :expire_after => 1200,
      :secret => settings.cookie_secret
  end

  configure do
    set :sessions, true
    set :redis, Redis.new
    set :views, Proc.new{ File.join(root, '..', 'views', self.to_s.downcase) }
    set :layouts, File.join(root, '..', 'views', 'layouts')

    Mail.defaults do
      delivery_method :smtp,
      address: Application.settings.smtp_server,
      port: Application.settings.smtp_port,
      openssl_verify_mode: OpenSSL::SSL::VERIFY_NONE
    end
  end

  helpers do
    # Overrides the default view lookup method for the purposes of using a shared
    # directory for layouts and per-controller directories for all other views
    def find_template(views, name, engine, &block)
      views = settings.layouts if name == :layout
      Array(views).each { |v| super(v, name, engine, &block) }
    end
  end

  before do
    unless ['/auth', '/auth/oauth'].include?(request.path)

      # Redirect un-authenticated users to login
      if session[:user_id].nil?
       redirect "#{request.scheme}://#{request.host_with_port}" +
                "/auth?path=#{request.path.split('/')[1]}"
      else

        # Check authorization of authenticated users
        if (settings.allowed_roles & session[:user_roles]).empty? &&
           !settings.allowed_roles.include?('All')
          halt 401
        end
      end
    end
  end

  get '/application' do
    redirect '/'
  end

  get '/' do
    slim :index
  end
end
