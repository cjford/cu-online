class MetroEnrollments < Application
  config_file self.config_path

  get '/' do
    slim :index
  end
end
