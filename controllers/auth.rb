class Auth < Application
  get '/' do
    # Redirect to Canvas for OAuth authentication
    redirect_uri = "#{request.scheme}://#{request.host_with_port}/auth/oauth"
    redirect_params = "client_id=#{settings.client_id}&response_type=code&state=#{params['path']}&redirect_uri=#{redirect_uri}"
    redirect to("https://ucdenver.instructure.com/login/oauth2/auth?#{redirect_params}")
  end

  get '/oauth' do
    # Redirected from Canvas with code. Add credentials and request token
    payload = {
      :code => params['code'],
      :client_id => settings.client_id,
      :client_secret => settings.client_secret
    }
    response = JSON.parse(RestClient.post("https://ucdenver.instructure.com/login/oauth2/token", payload))

    session[:user_id] = response['user']['id']
    session[:access_token] = response['access_token']
    set_roles(session[:user_id])

    # Retreive email of logged in user for sending reports
    profile = JSON.parse(RestClient.get("#{settings.api_base}/users/#{full_id(session[:user_id])}/profile", {Authorization: "Bearer #{settings.canvas_token}"}))
    session[:user_email] = profile['primary_email']

    redirect ('/' + params['state'])
  end

  get '/logout' do
    RestClient.delete("https://ucdenver.instructure.com/login/oauth2/token?access_token=#{session['access_token']}") unless session['access_token'].nil?
    session.clear
    erb :logout
  end
end
