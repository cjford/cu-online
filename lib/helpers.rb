module Sinatra
  module ApplicationHelper

    def full_id(short_id)
      short_id = short_id.to_s
      id = "1043"
      (13 - short_id.length).times{ id += "0"}
      id + short_id
    end

    def set_roles(user_id)
      # Account level roles
      request_uri = "#{settings.api_base}/accounts/#{settings.canvas_account_id}/admins?user_id[]=#{self.full_id(user_id)}"
      auth_header = {Authorization: "Bearer #{settings.canvas_token}"}
      session[:user_roles] = JSON.parse(RestClient.get(request_uri, auth_header)).collect{|user| user["role"]}

      # Course level roles
      query_string = %{
        SELECT distinct role_dim.name
        FROM role_dim
        JOIN enrollment_dim
          ON enrollment_dim.role_id = role_dim.id
        JOIN user_dim
          ON enrollment_dim.user_id = user_dim.id
        WHERE user_dim.canvas_id = #{user_id}}

      db = DBI.connect(settings.db_dsn, settings.db_user, settings.db_pwd)
      cursor = db.prepare(query_string)
      cursor.execute

      while row = cursor.fetch_hash
        session[:user_roles] << row["name"]
      end

      ensure cursor.finish if cursor
    end

  end
  helpers ApplicationHelper
end
